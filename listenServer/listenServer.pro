#-------------------------------------------------
#
# Project created by QtCreator 2014-01-22T10:01:06
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = listenServer
TEMPLATE = app


SOURCES += main.cpp\
        listenserver.cpp

HEADERS  += listenserver.h
QMAKE_CXXFLAGS += -std=c++11

RESOURCES += \
    images.qrc

TRANSLATIONS += main_ru.ts
