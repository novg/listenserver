#ifndef LISTENSERVER_H
#define LISTENSERVER_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTextEdit>
#include <QMessageBox>
#include <QBoxLayout>
#include <QLabel>
#include <QDir>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include <QValidator>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QApplication>
#include <QCheckBox>

/*!
 * \brief Класс служит для организации работы слушающего сокета, отображение
 * поступающих данных в графическом интерфейсе, сохранение их в лог-файл и
 * предоставляет некоторые пользовательские настройки в главном окне приложения,
 * которое при закрытии сворачивается в системный лоток.
 */

class ListenServer : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief Конструктор по умолчанию
     * \param nPort Номер порта
     * \param parent Указатель на объект-предок
     */
    explicit ListenServer(int nPort = 2112, QWidget *parent = 0);
    ~ListenServer();

public slots:
    /*!
     * \brief Вызывается каждый раз при соединении с новым клиентом.
     * Отправляет клиенту подтверждение об успешном соединении, устанавливает
     * связи между клиентом и приложением.
     */
    void slotNewConnection();
    /*!
     * \brief Производит получение данных от клиента с сохраниением в лог-файл
     * и вывод в главное окно программы.
     */
    void slotReadClient();
    /*!
     * \brief Определяет режим отображения главного окна программы.
     */
    void slotShowHide();
    /*!
     * \brief Служит для вывода информации о программе
     */
    void slotAbout();

private slots:
    void slotPathToLog();
    void slotShowClient();
    void slotChangePort();
    void slotChangeIcon();
    void slotShowMessages(int state);

private:
    void createTcpServer(int nPort);
    void sendToClient(QTcpSocket *pSocket, const QString &str);
    void createLogfile(const QString &filepath = "");
    void createWidgets();
    void readSettings();
    void writeSettings();
    void closeEvent(QCloseEvent*);
    void createAction();

private:
    int         port;
    QTcpServer  *tcpServer;
    QTextEdit   *textEdit;
    QPushButton *button;
    QLabel      *labelPath;
    QLabel      *labelClient;
    QLineEdit   *linePort;
    QCheckBox   *checkShowMessages;
    QFile       logfile;
    QSettings   settings;
    QSystemTrayIcon *trayIcon;
    QMenu       *trayIconMenu;
    bool        iconSwitcher;
    bool        isShow;
};

#endif // LISTENSERVER_H
