#include "listenserver.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTranslator translator;
    QString locale = QLocale::system().name();

    if (locale == "ru_RU") {
        translator.load("main_ru.qm", ".");
    }
    app.installTranslator(&translator);

    app.setOrganizationName("SibGUTI");
    app.setApplicationName("ListenServer");

    ListenServer w;
    w.setWindowIcon(QIcon("://images/socket.png"));
    QApplication::setQuitOnLastWindowClosed(false);

    return app.exec();
}
