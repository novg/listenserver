<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ListenServer</name>
    <message>
        <location filename="listenserver.cpp" line="48"/>
        <source>Path to log file</source>
        <translation>Путь к файлу журнала</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="50"/>
        <source>Connected with the Client:</source>
        <translation>Присоединен клиент: </translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="51"/>
        <source>&amp;Show messages</source>
        <translation>&amp;Показать сообщения</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="113"/>
        <source>Connected with the Client: </source>
        <translation>Присоединен клиент: </translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="121"/>
        <source>Connected with client: </source>
        <translation>Присоединен клиент: </translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="173"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="174"/>
        <source>Error opening file %1 for writing</source>
        <translation>Ошибка открытия файла %1 для записи</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="189"/>
        <source>Save log file</source>
        <translation>Сохранить файл журнала</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="191"/>
        <source>Log files (*.log);;All files (*)</source>
        <translation>Файлы журнала (*.log);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="248"/>
        <source>&amp;Show/Hide Application window</source>
        <translation>П&amp;оказать/Скрыть окно приложения</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="251"/>
        <source>&amp;About</source>
        <translation>О п&amp;рограмме</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="255"/>
        <source>&amp;Qiut</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="listenserver.cpp" line="265"/>
        <source>ListenServer</source>
        <translation></translation>
    </message>
</context>
</TS>
