#include "listenserver.h"

//------------------------------------------------------------------------------
ListenServer::ListenServer(int nPort, QWidget *parent)
    : QWidget(parent)
{
    port         = nPort;
    iconSwitcher = true;
    isShow       = false;

    readSettings();
    tcpServer = new QTcpServer(this);
    createTcpServer(port);
    createWidgets();
    createLogfile();

    connect(tcpServer, SIGNAL(newConnection()),
            this, SLOT(slotNewConnection()));
    connect(button, SIGNAL(clicked()),
            this, SLOT(slotPathToLog()));
    connect(linePort, SIGNAL(editingFinished()),
            this, SLOT(slotChangePort()));
    connect(checkShowMessages, SIGNAL(stateChanged(int)),
            this, SLOT(slotShowMessages(int)));

    createAction();
}

//------------------------------------------------------------------------------
void ListenServer::slotChangePort()
{
    port = linePort->text().toInt();
    createTcpServer(port);
}

//------------------------------------------------------------------------------
ListenServer::~ListenServer()
{
    logfile.close();
    writeSettings();
}

//------------------------------------------------------------------------------
void ListenServer::createWidgets()
{
    textEdit = new QTextEdit;
    textEdit->setReadOnly(true);
    button = new QPushButton(tr("Path to log file"));
    labelPath = new QLabel("logfile: ");
    labelClient = new QLabel(tr("Connected with the Client:"));
    checkShowMessages = new QCheckBox(tr("&Show messages"));
    QLabel *labelPort = new QLabel("port: ");
    linePort = new QLineEdit;
    linePort->setFixedWidth(50);

    QValidator *validator = new QIntValidator(1, 100000, this);
    linePort->setValidator(validator);
    linePort->setText(QString::number(port));

    //Layout setup
    QHBoxLayout *phbxLayout = new QHBoxLayout;
    phbxLayout->addWidget(labelPort);
    phbxLayout->addWidget(linePort);
    phbxLayout->addWidget(new QLabel("  "));
    phbxLayout->addWidget(button);
    phbxLayout->addWidget(labelPath);
    phbxLayout->addStretch(1);

    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addWidget(checkShowMessages);
    bottomLayout->addStretch(1);
    bottomLayout->addWidget(labelClient);

    QVBoxLayout *pvbxLayout = new QVBoxLayout;
    pvbxLayout->addLayout(phbxLayout);
    pvbxLayout->addWidget(textEdit);
    pvbxLayout->addLayout(bottomLayout);
    setLayout(pvbxLayout);
}

//------------------------------------------------------------------------------
void ListenServer::createTcpServer(int nPort)
{
    if (tcpServer->isListening()) {
        tcpServer->close();
    }

    if (!tcpServer->listen(QHostAddress::Any, nPort)) {
        QMessageBox::critical(0,
                              "Server Error",
                              "Unable to start the Server:"
                              + tcpServer->errorString()
                              );
        tcpServer->close();
        return;
    }
}

//------------------------------------------------------------------------------
// Вызывается каждый раз при соединении с новым клиентом
void ListenServer::slotNewConnection()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();

    connect(clientSocket, SIGNAL(disconnected()),
            clientSocket, SLOT(deleteLater()));
    connect(clientSocket, SIGNAL(readyRead()),
            this, SLOT(slotReadClient()));
    connect(clientSocket, SIGNAL(disconnected()),
            this, SLOT(slotShowClient()));

    sendToClient(clientSocket, "Server Response: Connected!");
    labelClient->setText(tr("Connected with the Client: ")
                         + clientSocket->peerAddress().toString());
    slotChangeIcon();
}

//------------------------------------------------------------------------------
void ListenServer::slotShowClient()
{
    labelClient->setText(tr("Connected with client: "));
    slotChangeIcon();
}

//------------------------------------------------------------------------------
void ListenServer::slotReadClient()
{
    // Получаем объект сокета, который вызвал данный слот
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QByteArray buff;

    buff.append(clientSocket->readAll());
    QString str = buff.data();

    if (isShow) {
        textEdit->moveCursor(QTextCursor::End);
        textEdit->insertPlainText(str.toUtf8());
    }

    logfile.write(str.toUtf8());
    logfile.flush();
}

//------------------------------------------------------------------------------
void ListenServer::sendToClient(QTcpSocket *pSocket, const QString &str)
{
    QByteArray buff = str.toLocal8Bit();
    pSocket->write(buff);
}

//------------------------------------------------------------------------------
void ListenServer::createLogfile(const QString &filepath)
{
    QString logname = filepath;

    if (filepath == "") {
        QString dirname = "log";
        logname = dirname + "/calls.log";

        QDir logdir(dirname);
        if (!logdir.exists()) {
            QDir::current().mkdir(dirname);
        }
    }

    if (logfile.isOpen())
        logfile.close();

    if (!logfile.isOpen()) {
        logfile.setFileName(logname);
        if (!logfile.open(QIODevice::Append)) {
            QMessageBox::warning(0,
                                 tr("Warning"),
                                 tr("Error opening file %1 for writing")
                                 .arg(logfile.fileName())
                                 );
            return;
        }
    }

    labelPath->setText("logfile: " + logfile.fileName());
}

//------------------------------------------------------------------------------
void ListenServer::slotPathToLog()
{
    QString logname =
            QFileDialog::getSaveFileName(this,
                                         tr("Save log file"),
                                         "",
                                         tr("Log files (*.log);;All files (*)")
                                        );
    if (logname != "") {
        createLogfile(logname);
    }
}

//------------------------------------------------------------------------------
void ListenServer::writeSettings()
{
    settings.beginGroup("/connect");
    settings.setValue("/port", port);
    settings.endGroup();
}

//------------------------------------------------------------------------------
void ListenServer::readSettings()
{
    settings.beginGroup("/connect");
    port = settings.value("/port", 2112).toInt();
    settings.endGroup();

}

//------------------------------------------------------------------------------
void ListenServer::closeEvent(QCloseEvent *)
{
    if (trayIcon->isVisible()) {
        hide();
    }
}

//------------------------------------------------------------------------------
void ListenServer::slotShowHide()
{
    setVisible(!isVisible());
}

//------------------------------------------------------------------------------
void ListenServer::slotAbout()
{
//    trayIcon->showMessage("For your information",
//                          "You have selected the "
//                          "\"Show Message!\" option",
//                          QSystemTrayIcon::Information,
//                          3000
//                          );
    QMessageBox::about(0, "About", "ListenServer Ver. 0.1.\n"
                       "Выпускная работа студента СибГУТИ\n"
                       "Новгородского Александра, ПБВ-11"
                       );
}

//------------------------------------------------------------------------------
void ListenServer::createAction()
{
    QAction *actionShowHide =
            new QAction(tr("&Show/Hide Application window"), this);
    connect(actionShowHide, SIGNAL(triggered()), this, SLOT(slotShowHide()));

    QAction *actionAbout = new QAction(tr("&About"), this);
    connect(actionAbout, SIGNAL(triggered()),
            this, SLOT(slotAbout()));

    QAction *actionQiut = new QAction(tr("&Qiut"), this);
    connect(actionQiut, SIGNAL(triggered()), qApp, SLOT(quit()));

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(actionShowHide);
    trayIconMenu->addAction(actionAbout);
    trayIconMenu->addAction(actionQiut);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setToolTip(tr("ListenServer"));

    slotChangeIcon();

    trayIcon->show();
}

//------------------------------------------------------------------------------
void ListenServer::slotChangeIcon()
{
    iconSwitcher = !iconSwitcher;
    QString strPixmapName = iconSwitcher ? "://images/socket.png"
                                         : "://images/linneighborhood.png";
    trayIcon->setIcon(QPixmap(strPixmapName));
}

//------------------------------------------------------------------------------
void ListenServer::slotShowMessages(int state)
{
    if (state != 0) {
        isShow = true;
    }
    else {
        isShow = false;
        textEdit->clear();
    }
}
